<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title;?></title>

        <link rel="stylesheet" href="assets/css/reset.css">
        <link rel="stylesheet" href="assets/css/style.css" />
        <?php
        if (isset($css_inc) && $css_inc != NULL) {
            foreach ($css_inc as $value) {
                echo '<link rel="stylesheet" href="assets/css/' . $value . '.css">';
            }
        }
        ?> 

        <link rel="shortcut icon" href="favicon.ico">
        
        <!--[if lt IE 9]>
            <script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
        <![endif]-->
    </head>
    <body <?php echo ($page_id) ? ' id="page_'. $page_id .'"' : ''; ?>>
        <!--[if lt IE 9]>
            <div class="outdatedBrowserOverlay overlay"><p class="outdatedBrowserBox">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p></div>
        <![endif]-->
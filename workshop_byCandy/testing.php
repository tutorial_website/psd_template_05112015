<?php   
    $page = 'home';
    $css_inc = array('gradient.css');
    include 'inc/header.php';
    $js_inc = array('gradient.js')
?>

<div class="container">
    <h1><strong>HTML &amp; CSS Slicing Tutorial Workshop</strong></h1>

    <p class="red">I have created a "html template" for your references, you may check around.</p>

    <P>Require Material Below</P>

    <ol>
        <li>Before you start, pick 1 of the PSD template and start slicing tutorial workshop</li>

        <li>Use only HTML &amp; CSS code, save your index as PHP file. <span class="red">Avoid using index.html</span> <strong>You may view your PHP by Vhost</strong>

            <h2><a href="https://delanomaloney.com/2013/07/how-to-set-up-virtual-hosts-using-xampp/" target="_blank">how to create virtual host on XAMPP</a></h2>

            <ol class="padding_left">
                <li>You need to have <a href="https://www.apachefriends.org/index.html" target=_blank>XAMPP installed</a></li>

                <li>
                    Navigate to <strong>C:\xampp\apache\conf\extra\</strong> folder open up <strong>httpd-vhosts.conf</strong> text editor with administrative privileges / Open up in Sublime
                </li>

                <li>
                    Find <strong>NameVirtualHost *:80</strong> - Remove <strong>##</strong>
                </li>
 
                <li>
                    Copy and Paste the code below to the most bottom
                    <br>
                    This is to view localhost
                    
                    <p>
                        <strong>
                            &lt;VirtualHost *:80&gt;<br>
                                DocumentRoot "C:/xampp/htdocs/"<br>
                                ServerName localhost<br>
                                ServerAlias localhost<br>
                            &lt;/VirtualHost&gt;
                        </strong>
                    </p>

                    <p>
                        This is to view your PHP on Vhost<br>
                        <strong>
                            &lt;VirtualHost *:80&gt;<br> 
                                DocumentRoot "C:/xampp/htdocs/folder_name" <br>
                                ServerName folder_name.dev <br>
                                ServerAlias www.folder_name.dev <br>
                            &lt;/VirtualHost&gt;
                        </strong>
                    </p>

                    <p>
                        This code is comment for further use <br>
                        <strong>##127.0.0.1  folder_name.dev  www.folder_name.dev</strong>
                    </p>
                </li>

                <li>
                    Now we head over to our Windows Hosts File, to edit the HOSTS. the file will be located at <strong>C:/Windows/System32/drivers/etc/hosts</strong>, where hosts is the file.
                    open up note pad with administrative privileges / Open up in Sublime

                    <p>
                        Some of you need to un-comment <strong>#</strong><br>
                        127.0.0.1   localhost<br>
                        ::1         localhost
                    </p>

                    <p>
                        Paste this to the most bottom of the host file<br>
                        <strong>
                            127.0.0.1  folder_name.dev  www.folder_name.dev
                        </strong>
                    </p>
                </li>
                
                <li>
                    Paste URL <strong>folder_name.dev</strong> to any browser to view your PHP as Vhost 
                </li>
            </ol>
        </li>

        <li>
            <span class="red">Must</span> Update your file into Bitbucket (Source Tree)

            <ol class="padding_left">
                <li>
                    <strong>Commit</strong> all your changes &amp; Please give a proper Message

                    <img src="assets/img/bit_1.jpg" alt="">
                </li>

                <li><strong>fetch</strong> To grab the latest updates</li>

                <li><strong>pull</strong> To merge your file with other &amp; Remeber always PULL before you continue your work
                    <img src="assets/img/bit_2.jpg" alt="">
                </li>

                <li><strong>push</strong> Share your file with everyone</li>
            </ol>
        </li>

        <li><a href="http://learn.shayhowe.com/">Learn to Code HTML &amp; CSS</a>
        </li>

        <li>
            start a templates
            <a href="http://www.initializr.com/" target="_blank">Html Boilerplate templates</a>
        </li>

        <li><span class="red">Must have</span> reset / normalize .css</li>

        <li>
            <span class="red">Must have</span> Image Sprites css - <a href="http://www.spritecow.com/" target="_blank">sprite cow</a>
        </li>

        <li><span class="red">Must have</span> 
            <a href="http://sass-lang.com/guide" target="_blank">SCSS Syntax -</a>
            <strong>include mixins &amp; variables</span></strong>
        </li>

        <li><span class="red">Must have</span> CSS shorthand</li>

        <li><span class="red">Must have</span> 
            <a href="http://www.font-face.com/#green_content" target="_blank">Custom Font - @font-face</a>
        </li>

        <li><span class="red">Must have</span> 
            <a href="http://fontello.com/" target="_blank">Fontello - icon fonts generator</a> <strong class="red">avoid using images as icon</strong> use 
            <a href="https://codyhouse.co/gem/animate-svg-icons-with-css-and-snap/" target="_blank">SVG convert as Icon</a>
        </li>

        <li>Browser Support - google chrome, safari, firefox, IE11-IE9 <strong class="red">IE8 below show popup brower not support</strong></li>

        <li>
            Inspect Element is your Best Friend - 
            <a href="http://getfirebug.com/">FireBug for Firefox</a>
        </li>

        <li class="red">Avoid use inline CSS</li>

        <li class="red">Avoid using Bootstrap (only for this project)</li>

        <li><span class="red">No Responsive (only for this project)</span> if you have time you may Do Responsive</li>
    </ol>

    <p>If you have question can always ask.</p>

    <p>Once you done your slicing, we can discuss together what can be improved. Thanks.</p>

    <p>PS: You can explore more if you have free time or share your knowledge with others.</p>
    
    <div id="url_references">
        <h1>References URL</h1>

        <ol>
            <li><a href="https://placehold.it/" target="_blank">quick and simple image placeholder </a></li>

            <li><a href="https://www.atlassian.com/git/tutorials/" target="_blank">Become a git guru.</a></li>

            <li><a href="https://blog.generalassemb.ly/sublime-text-3-tips-tricks-shortcuts/" target="_blank">SUBLIME TEXT 3 - TIPS</a></li>

            <li><a href="http://www.cssfontstack.com/" target="_blank">CSS font stacks</a></li>
        </ol>
    </div>
</div>

<?php include 'inc/footer.php'; ?>